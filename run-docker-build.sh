#!/usr/bin/env bash

# fail script if any command failed
set -e

CI_REGISTRY_IMAGE=registry.gitlab.com/cloudstark/images/kafka-connector-jdbc
VERSION=5.2.1

BUILD_ENV=${CI:-"LOCAL"}
PUSH_ENABLED=${PUSH_ENABLED:-false}
SKIP_TESTS=${SKIP_TESTS:-true}

GIT_COMMIT_ID=$(git show -s --format=%h HEAD)
GIT_COMMIT_TS=$(git show -s --format=%cI HEAD)
GIT_COMMIT_TS=${GIT_COMMIT_TS//[:,-]/''}
GIT_COMMIT_TS=${GIT_COMMIT_TS:0:15}

# Gitlab sets env variable CI to "true" but this is not a memorably value for our purposes
if [ "$BUILD_ENV" == 'true' ]; then
  BUILD_ENV="CI"
fi

echo "BUILD_ENVIRONMENT: $BUILD_ENV"
echo "VERSION          : $VERSION"
echo "GIT_COMMIT_ID    : $GIT_COMMIT_ID"
echo "GIT_COMMIT_TS    : $GIT_COMMIT_TS"
echo "SKIPTESTS        : $SKIP_TESTS"
echo "PUSH_ENABLED     : $PUSH_ENABLED"

if [[ -n $(git status --porcelain) ]]; then
  if [ "$BUILD_ENV" != 'CI' ]; then
    echo "!!! repo is dirty -> setting BUID_ENVIRONMENT to DIRTY !!!";
    BUILD_ENV="DIRTY"
  fi
fi

BUILD_NUMBER="local"

case "$BUILD_ENV" in
  "LOCAL")
    echo "we are running in a local build environment"
    BUILD_NUMBER="local"
    ;;
  "DIRTY")
    echo "we are running in a dirty build environment"
    BUILD_NUMBER="dirty"
    ;;
  "CI")
    echo "we are running in a CI build environment"
    BUILD_NUMBER="${VERSION}"
    ;;
  *)
    echo "unknown environment"
    exit 1
    ;;
esac

echo "build jar-file"
mvn clean package -DskipTests=$SKIP_TESTS

echo "build Docker image"
docker image build \
  --build-arg KAFKA_CONNECT_JDBC_VERSION=$VERSION \
  -f src/main/docker/Dockerfile  \
  -t $CI_REGISTRY_IMAGE:$VERSION \
  .

if [ "$PUSH_ENABLED" == 'true' ]; then
  docker image push $CI_REGISTRY_IMAGE:$VERSION
else
  echo "docker push not enabled";
fi
